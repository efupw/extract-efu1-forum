<?php

$text_q = null;
$author_q = null;

if (isset($_REQUEST['text'])) {
    $text_q = $_REQUEST['text'];
}

if (isset($_REQUEST['author'])) {
    $author_q = $_REQUEST['author'];
}

function foo(?string $t): string {
    return empty($t) ? '' : ('value="' . htmlspecialchars($t) . '"');
}

?><!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https:&#x2F;&#x2F;efupw.com&#x2F;efu1-forum/style.css"/>
    <title>Search EFU1 forum</title>
</head>
<body>

<div class="content">

<form action="" method="get">
  <label for="text">Look for</label>
  <input type="text" name="text" id="text" placeholder="anything"
         <?= foo($text_q); ?>/>
  <label for="author">by</label>
  <input type="text" name="author" id="author" placeholder="anybody"
         <?= foo($author_q); ?>/>
  <input type="submit" value="Go!" />
</form>

<?php
$db = new PDO('sqlite:finddb.sqlite3');

$sql = 'select * from post';
if ($text_q) {
    $sql .= ' join (select post_id from post_t where post_body match :body order by rank, post_id) using (post_id)';
}
if ($author_q) {
    $sql .= ' join (select post_id from post_t where post_author match :author order by rank, post_id) using (post_id)';
}
$sql .= ' order by post_id limit 100;';

$stmt = $db->prepare($sql);
if ($text_q) {
    $stmt->bindParam(':body', $text_q);
}
if ($author_q) {
    $stmt->bindParam(':author', $author_q);
}
$stmt->execute();

function render_term(?string $t, string $default): string {
    return empty($t) ? $default : ('<var>' . htmlspecialchars($t) . '</var>');
}

function nl2para(string $t): string {
    return  '<p>' . implode('</p><p>', explode('\n', $t)) . '</p>';
}

?>
<h2>Top 100 posts matching <?= render_term($text_q, 'anything') ?> by <?= render_term($author_q, 'anybody') ?>:</h2>
<?php while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { ?>
  <div id="<?= $row['post_id'] ?>" class="post">
    <div class="post--meta">
      <p class="post--author"><?= $row['post_author'] ?></p>
      <p class="post--time"><?= $row['when'] ?></p>
      <p class="post--permalink">
        <a href="<?= $row['post_path'] ?>">#<?= $row['post_id'] ?></a>
      </p>
    </div><div class="post--body"><?= nl2para($row['post_body']) ?></div>
  </div>
<?php } ?>
</div>
</body>
