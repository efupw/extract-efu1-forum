#!/usr/bin/env python3

import bs4
import csv
import sys
import pathlib

forum_extract_root = sys.argv[1]
index_file = sys.argv[2]

with open(index_file, "w", newline="") as outfile:
    spamwriter = csv.writer(outfile)
    record = [
            "post_id",
            "post_path",
            "forum_name",
            "topic_title",
            "post_author",
            "when",
            "post_body",
            ]
    spamwriter.writerow(record)
    root = f"{forum_extract_root}/topic/"
    #root = f"{root}/24/24056/encyclopaedia-dunwarrica/"
    for f in pathlib.Path(root).rglob("*.html"):
        soup = bs4.BeautifulSoup(f.read_text(), features="html.parser")

        try:
            forum_name = soup.p.text
            topic_title = soup.find("h2").text
            posts = meta = soup.find_all("div", class_="post")
            for post in posts:
                try:
                    meta = post.find("div", class_="post--meta")

                    post_author = meta.find("p", class_="post--author").text
                    when = meta.find("p", class_="post--time").text
                    post_body = post.find("div", class_="post--body").text
                    permalink = meta.find("p", class_="post--permalink")
                    frag = permalink.find("a").get("href")
                    post_path = f"/{f}{frag}"
                    post_id = frag[6:]

                    record = [post_id, post_path, forum_name, topic_title, post_author, when, post_body,]
                    spamwriter.writerow(record)
                except Exception as e:
                    print(e)
                    print(f)
        except Exception as e:
            print(e)
            print(f)
