These scripts can generate an SQLite3 FTS database from a copy of the static Efu 1 forum archive.

Enable the Python virtual environment:

```sh
$ python3 -m venv venv
$ . venv/bin/activate
$ (venv) python3 -m pip install -r requirements.txt
```

Index the public forum:

```sh
$ (venv) tar xf efu1public.tgz --strip-components=1
$ (venv) ./index-forum-extract.py efu1-forum forum-public-index.csv
$ (venv) ./load-index-into efu1-forum forum-public-index.csv
```

Index the private forum:

```sh
$ (venv) tar xf efu1private.tgz --strip-components=1
$ (venv) ./index-forum-extract.py efu1-forum-private forum-private-index.csv
$ (venv) ./load-index-into efu1-forum-private forum-private-index.csv
```

Upload the public forum index:

```sh
$ ssh efupw.com 'chmod u+w efupw.com/efu1-forum'
$ scp find.php efupw.com:efupw.com/efu1-forum/find.php
$ scp efu1-forum/finddb.sqlite3 efupw.com:efupw.com/efu1-forum/finddb.sqlite3
$ ssh efupw.com 'chmod u-w efupw.com/efu1-forum'
```

Upload the private forum index:

```sh
$ ssh efupw.com 'chmod u+w efupw.com/efu1-forum-private'
$ scp find.php efupw.com:efupw.com/efu1-forum-private/find.php
$ scp efu1-forum-private/finddb.sqlite3 efupw.com:efupw.com/efu1-forum-private/finddb.sqlite3
$ ssh efupw.com 'chmod u-w efupw.com/efu1-forum-private'
```
