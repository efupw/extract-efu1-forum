# Schema overview

This document describes the (reverse-engineered) schema structure of the first
EfU forum, a phpBB2 installation. `schema.sql` contains a copy of that
structure, without any data, although that copy has been superficially
anonymized as described below.

You will need a working MySQL database server; see [Installing
MySQL](#installing-mysql). Then you should familiarize yourself with the
schema, and optionally consult [Examples](#examples) for ideas on what you can
do with it.

## Tables

This is an overview of the most interesting tables and how they relate to each
other.

- `phpbb_users`: All forum users.

    - `user_id`: the numerical user ID.
    - `username`: the unique user name.
    - `user_posts`: the number of posts this user has authored.
    
- `phpbb_posts`: An associative, or junction, table, that ties many other
  tables together. Most queries spanning multiple tables will also need to join
  this table.

    - `post_id`: reference to `phpbb_posts_text.post_id`; the numerical ID of
      the post content.
    - `topic_id`: reference to `phpbb_topics.topic_id`; the numerical ID of the
      thread title.
    - `forum_id`: reference to `phpbb_forums.forum_id`; the numerical ID of the
      sub-forum the post was posted in.
    - `poster_id`: reference to `phpbb_users.user_id`; the numerical ID of the
      user that authored the post.
    - `post_time`: the number of seconds since the Unix epoch; convert it into
      a human-friendly UTC time stamp with `FROM_UNIXTIME(post_time)`.
    - `post_username`: this column is always empty; it is documented here only
      to show that it is useless.

- `phpbb_posts_text`: The text contents of all posts.

    - `post_id`: the numerical post ID.
    - `post_text`: the post body.

- `phpbb_forums`: All sub-forum metadata.

    - `forum_id`: the numerical forum ID.
    - `forum_name`: the name of the forum.

- `phpbb_topics`: All thread metadata.

    - `topic_id`: the numerical thread ID.
    - `topic_title`: the name of the thread.
    - `forum_id`: reference to `phpbb_forums.forum_id.`

- `phpbb_privmsgs`: Like `phpbb_posts`, an associative table, that ties many
  other tables together. Most queries spanning multiple tables will also need
  to join this table.

    - `privmsgs_id`: reference to `phpbb_privmsgs_tex.privmsgs_text_id`; the
      numerical ID of the message content.
    - `privmsgs_date`: the number of seconds since the Unix epoch; see
      `phpbb_posts.post_time`.
    - `privmsgs_from_userid`: reference to `phpbb_users.user_id`; the numerical
      ID of the user that sent the message.
    - `privmsgs_to_userid`: reference to `phpbb_users.user_id`; the numerical
      ID of the user that received the message.

- `phpbb_privmsgs_text`:

    - `privmsgs_text_id`: the numerical message ID.
    - `privmsgs_text`: the message body.

## Examples

Extract the 10 most chatty users:

```sql
SELECT username, user_posts
FROM phpbb_users
ORDER BY user_posts DESC LIMIT 10;
```

Extract any 3 posts by "Arkov" that contain the word "entstehung":

```sql
SELECT
    u.username,
    f.forum_name,
    t.topic_title,
    FROM_UNIXTIME(p.post_time) AS post_time,
    c.post_text
FROM phpbb_posts  AS p
JOIN phpbb_topics AS t USING(topic_id)
JOIN phpbb_users  AS u ON u.user_id = p.poster_id
JOIN phpbb_forums AS f ON p.forum_id = f.forum_id
JOIN phpbb_posts_text AS c USING(post_id)
WHERE u.username = 'Arkov'
AND post_text LIKE '%entstehung%'
LIMIT 3;
```

Reconstruct the complete thread with ID 23121, posts ordered chronologically:

```sql
SELECT
    u.username,
    t.topic_title,
    FROM_UNIXTIME(p.post_time) AS post_time,
    c.post_text
FROM phpbb_posts  AS p
JOIN phpbb_topics AS t USING(topic_id)
JOIN phpbb_users  AS u ON u.user_id = p.poster_id
JOIN phpbb_posts_text AS c USING(post_id)
WHERE p.topic_id = 23121
ORDER BY p.post_id;
```

Private messages are not structured threads so reconstructing dialogue via
private messages is very difficult. By default, phpBB kept the first message's
subject and prefixed it with "Re:" when responding and this can be a helpful
trick for reconstructing some messages, but since users could freely change the
subject it's not completely reliable. Here is an example that uses this trick:

```sql
SELECT
    p.privmsgs_id,
    from_unixtime(p.privmsgs_date),
    ufrom.username AS sender,
    uto.username AS recipient,
    p.privmsgs_subject,
    t.privmsgs_text
FROM phpbb_privmsgs_text AS t
JOIN phpbb_privmsgs AS p     ON p.privmsgs_id = t.privmsgs_text_id
JOIN phpbb_users    AS ufrom ON ufrom.user_id = p.privmsgs_from_userid
JOIN phpbb_users    AS uto   ON uto.user_id   = p.privmsgs_to_userid
WHERE privmsgs_subject LIKE '%lower''s history'
ORDER BY privmsgs_id;
```

# Installing MySQL

How you install MySQL is largely irrelevant; choose whichever method suits you
best. MySQL 5.7 is known to work with the dump but other versions may also.
Most MariaDB versions should also work.

MySQL Workbench should work work on all major platforms. Linux distributions
also typically have easy access to the latest MySQL via their package managers,
e.g.

```sh
$ sudo apt install mysql
```

## Dockerized MySQL

If you happen to be comfortable with Docker you can use the following command
to quickly create a MySQL 5.7 database with user "root" and password "root":

```sh
$ docker run \
    --detach \
    --name efuforum \
    --restart=unless-stopped \
    --publish 3306:3306 \
    --env LANG=C.UTF-8 \
    --env MYSQL_ROOT_PASSWORD=root \
    mysql:5.7 
```

After about 30 seconds you can create a new database and load the schema with

```sh
$ docker exec --interactive efuforum mysql -uroot -proot -e 'CREATE DATABASE forum'
$ docker exec --interactive efuforum mysql -uroot -proot forum < schema.sql
```

Now you can access the MySQL CLI with

```sh
$ docker exec --interactive --tty efuforum mysql -uroot -proot forum 
```

Alternatively, it may be possible to access from MySQL Workbench via Docker's
"Gateway" IP address, which is usually 172.17.0.1:

```sh
docker inspect efuforum | grep '"Gateway"'
        "Gateway": "172.17.0.1",
                "Gateway": "172.17.0.1",
```

## Anonymization

To protect users against leaks of personal information, the following
statements were executed on the original schema:

```sql
ALTER TABLE phpbb_banlist DROP COLUMN ban_email;
ALTER TABLE phpbb_banlist DROP COLUMN ban_ip;
ALTER TABLE phpbb_posts DROP COLUMN poster_ip;
ALTER TABLE phpbb_privmsgs DROP COLUMN privmsgs_ip;
ALTER TABLE phpbb_sessions DROP COLUMN session_ip;
ALTER TABLE phpbb_sessions_keys DROP COLUMN last_ip;
ALTER TABLE phpbb_users DROP COLUMN user_actkey;
ALTER TABLE phpbb_users DROP COLUMN user_aim;
ALTER TABLE phpbb_users DROP COLUMN user_email;
ALTER TABLE phpbb_users DROP COLUMN user_from;
ALTER TABLE phpbb_users DROP COLUMN user_icq;
ALTER TABLE phpbb_users DROP COLUMN user_msnm;
ALTER TABLE phpbb_users DROP COLUMN user_newpasswd;
ALTER TABLE phpbb_users DROP COLUMN user_password;
ALTER TABLE phpbb_users DROP COLUMN user_website;
ALTER TABLE phpbb_users DROP COLUMN user_yim;
ALTER TABLE phpbb_vote_voters DROP COLUMN vote_user_ip;
```

Additionally, the original included a number of tables from a failed forum
upgrade experiment. These were all uninteresting; they contained mostly no
data, but users had been migrated. Those tables were deleted with a single
`DROP TABLE` statement generated by these two statements;

```sql
SET SESSION group_concat_max_len = 99999999;
SELECT CONCAT('DROP TABLE ', GROUP_CONCAT(table_name), ';') AS drop_stmt
FROM information_schema.tables
WHERE table_name LIKE 'phpbb3_%' OR table_name = 'to_e107_tmp';
```

In the data, original user nicknames were preserved, partly for data integrity,
partly because they occurred so frequently inside message bodies that erasing
them would have been infeasible. Finally, the copy was cleaned of email
address-like strings with the following Python script:

```python
#!/usr/bin/env python3

import re
import sys

replacements = {}

def repl(matchobj):
    m = matchobj[0]
    if m not in replacements:
        replacements[m] = ('redacted-%s@domain.example') % (len(replacements) + 1)
    return replacements[m]
    
with open(sys.argv[1]) as f:
    regex = re.compile("[-.\w]+@[-.\w]+(?:\.\w+)+")
    for l in f:
        l = re.sub(regex, repl, l)
        print(l)
```

The data cleaning did not affect the schema.
