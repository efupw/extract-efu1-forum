SELECT
    cat_id,
    cat_title,
    forum_id,
    forum_name
FROM phpbb_forums
JOIN phpbb_categories USING(cat_id)
-- Main Forums
-- Information
-- In-Character Forums
-- Realmslore
-- Metafactions
WHERE cat_id IN (1, 3, 6, 8, 14)
ORDER BY cat_order, forum_order, forum_id;
