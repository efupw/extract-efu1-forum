SELECT
    cat_id,
    cat_title,
    forum_id,
    forum_name
FROM phpbb_forums
JOIN phpbb_categories USING(cat_id)
ORDER BY cat_order, forum_order;
