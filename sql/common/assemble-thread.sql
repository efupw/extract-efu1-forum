SELECT
    u.username,
    p.post_id,
    t.topic_id,
    f.forum_id,
    f.forum_name,
    t.topic_title,
    FROM_UNIXTIME(p.post_time) AS post_time,
    c.post_text
FROM phpbb_posts  AS p
JOIN phpbb_topics AS t USING(topic_id)
JOIN phpbb_users  AS u ON u.user_id = p.poster_id
JOIN phpbb_posts_text AS c USING(post_id)
JOIN phpbb_forums AS f ON f.forum_id = t.forum_id
WHERE p.topic_id = :topic_id
ORDER BY p.post_id;
