SELECT
    f.forum_id,
    t.topic_id,
    t.topic_type,
    FROM_UNIXTIME(t.topic_time) AS topic_time,
    u.username,
    t.topic_title,
    f.forum_name,
    f.forum_desc
FROM phpbb_topics AS t
INNER JOIN phpbb_forums AS f USING(forum_id)
INNER JOIN phpbb_users  AS u ON u.user_id = t.topic_poster
WHERE f.forum_id = :forum_id
ORDER BY topic_type DESC, topic_id DESC;
