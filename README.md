# extract-efu1-forum

Given a running MySQL server with a backup of the original EfU forum, this
binary generates a forum archive as static HTML files.

Copy `dist.settings.toml` to a new path, then modify the copy as necessary. Run
the binary, passing this new file as the only argument:

```sh
$ cargo run --release -- mycfg.toml
```

The output directory is completely self-contained and is ready to be served from
a Web server; for instance, while testing, with

```sh
$ cd forum-public/
$ python3 -m http.server 8000
```

The binary has a concept of a "public" archive. All this does is decide how
many forums to extract. A public archive includes only those forums that were
open to all users, whereas a non-public archive includes _all forums_. The
latter includes a lot of sensitive information, and if hosted on the Internet
should be access-protected, for instance using an `.htaccess` file such as the
sample `sample-htaccess` included in this repository.

This repository includes the expected schema but unfortunately not sample data.
