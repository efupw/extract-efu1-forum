use mysql::chrono::NaiveDateTime;

pub struct Sql<'a> {
    pub forum_names: &'a str,
    pub topic_names: &'a str,
    pub post_topics: &'a str,
    pub list_forums: &'a str,
    pub list_threads: &'a str,
    pub assemble_thread: &'a str,
}

impl<'a> Sql<'a> {
    pub fn public() -> Self {
        Self {
            forum_names: include_str!("../sql/common/forum-names.sql"),
            topic_names: include_str!("../sql/common/topic-names.sql"),
            post_topics: include_str!("../sql/common/post-topics.sql"),
            list_forums: include_str!("../sql/public/list-forums.sql"),
            list_threads: include_str!("../sql/common/list-threads.sql"),
            assemble_thread: include_str!("../sql/common/assemble-thread.sql"),
        }
    }

    pub fn private() -> Self {
        Self {
            forum_names: include_str!("../sql/common/forum-names.sql"),
            topic_names: include_str!("../sql/common/topic-names.sql"),
            post_topics: include_str!("../sql/common/post-topics.sql"),
            list_forums: include_str!("../sql/private/list-forums.sql"),
            list_threads: include_str!("../sql/common/list-threads.sql"),
            assemble_thread: include_str!("../sql/common/assemble-thread.sql"),
        }
    }
}

#[derive(Debug)]
pub struct ForumName {
    pub forum_id: i32,
    pub forum_name: String,
}

#[derive(Debug)]
pub struct Forum {
    pub cat_id: i32,
    pub cat_title: String,
    pub forum_id: i32,
    pub forum_name: String,
}

#[derive(Debug)]
pub struct TopicName {
    pub topic_id: i32,
    pub topic_title: String,
}

#[derive(Debug)]
pub struct Topic {
    pub forum_id: i32,
    pub topic_id: i32,
    pub topic_type: i8,
    pub topic_time: NaiveDateTime,
    pub username: String,
    pub topic_title: String,
    pub forum_name: String,
    pub forum_description: String,
}

#[derive(Debug)]
pub struct PostTopic {
    pub post_id: i32,
    pub topic_id: i32,
}

#[derive(Debug)]
pub struct Post {
    pub username: String,
    pub post_id: i32,
    pub topic_id: i32,
    pub forum_id: i32,
    pub forum_name: String,
    pub topic_title: String,
    pub post_time: NaiveDateTime,
    pub body: String,
}

impl ForumName {
    pub fn from_row(row: mysql::Row) -> Self {
        let (forum_id, forum_name) = mysql::from_row(row);
        Self {
            forum_id,
            forum_name,
        }
    }
}

impl Forum {
    pub fn from_row(row: mysql::Row) -> Self {
        let (cat_id, cat_title, forum_id, forum_name) = mysql::from_row(row);
        Self {
            cat_id,
            cat_title,
            forum_id,
            forum_name,
        }
    }
}

impl TopicName {
    pub fn from_row(row: mysql::Row) -> Self {
        let (topic_id, topic_title) = mysql::from_row(row);
        Self {
            topic_id,
            topic_title,
        }
    }
}

impl Topic {
    pub fn from_row(row: mysql::Row) -> Self {
        let (
            forum_id,
            topic_id,
            topic_type,
            topic_time,
            username,
            topic_title,
            forum_name,
            forum_description,
        ) = mysql::from_row(row);
        Self {
            forum_id,
            topic_id,
            topic_type,
            topic_time,
            username,
            topic_title,
            forum_name,
            forum_description,
        }
    }
}

impl PostTopic {
    pub fn from_row(row: mysql::Row) -> Self {
        let (post_id, topic_id) = mysql::from_row(row);
        Self { post_id, topic_id }
    }
}

impl Post {
    pub fn from_row(row: mysql::Row) -> Self {
        let (username, post_id, topic_id, forum_id, forum_name, topic_title, post_time, body) =
            mysql::from_row(row);
        Self {
            username,
            post_id,
            topic_id,
            forum_id,
            forum_name,
            topic_title,
            post_time,
            body,
        }
    }
}
