#[derive(Debug, Serialize)]
pub struct Category {
    pub cat_id: i32,
    pub cat_title: String,
    pub forums: Vec<Forum>,
}

#[derive(Debug, Serialize)]
pub struct Forum {
    pub forum_id: i32,
    pub forum_name: String,
    pub forum_path: String,
}

#[derive(Debug, Serialize)]
pub struct Topic {
    pub forum_id: i32,
    pub topic_id: i32,
    pub topic_type: i8,
    pub username: String,
    pub topic_title: String,
    pub topic_time: String,
    pub forum_name: String,
    pub forum_description: String,
    pub topic_path: String,
}

#[derive(Debug, Serialize)]
pub struct Post {
    pub username: String,
    pub post_id: i32,
    pub topic_id: i32,
    pub topic_title: String,
    pub forum_name: String,
    pub forum_path: String,
    pub post_time: String,
    pub body: String,
}

#[cfg_attr(feature = "cargo-clippy", allow(clippy::needless_pass_by_value))]
pub mod filter {
    use regex::Regex;
    use std::collections::HashMap;
    use tera::Result;
    use tera::Value;

    lazy_static! {
        static ref RE_NL: Regex = { Regex::new(r#"(?:\r?\n){2,}"#).unwrap() };
    }

    pub fn nl_to_paragraph(v: Value, _h: HashMap<String, Value>) -> Result<Value> {
        if v.is_string() {
            let s = v.as_str().unwrap();
            let s = RE_NL.replace_all(s, "</p><p>");
            Ok(json!(format!("<p>{}</p>", s)))
        } else {
            Ok(v)
        }
    }

}
