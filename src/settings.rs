use config::Config;
use config::ConfigError;
use config::File;

#[derive(Debug, Deserialize)]
struct Mysql {
    user: String,
    password: String,
    host: String,
    port: String,
    database: String,
}

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub base_url: String,
    pub out_dir: String,
    pub public: bool,
    mysql: Mysql,
}

impl Settings {
    pub fn new(path: &str) -> Result<Self, ConfigError> {
        let mut s = Config::new();
        s.merge(File::with_name(path))?;
        s.try_into()
    }

    pub fn database_url(&self) -> String {
        let m = &self.mysql;
        format!(
            "mysql://{}:{}@{}:{}/{}",
            m.user, m.password, m.host, m.port, m.database
        )
    }
}
