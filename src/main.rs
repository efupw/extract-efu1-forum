///
/// index.html
/// - list all forums by categories
///
/// forums/<id>/<puny>/index.html
/// - list all threads by id asc
///
/// threads/<id>/<puny>/index.hml
/// - embed all posts by id asc
///
///
extern crate config;
#[macro_use]
extern crate clap;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate mysql;
extern crate regex;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;
extern crate rayon;
extern crate tera;

mod schema;
mod settings;
mod view;

use clap::App;
use clap::Arg;
use mysql::chrono::DateTime;
use mysql::chrono::Utc;
use rayon::prelude::*;
use regex::Regex;
use settings::Settings;
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;

fn main() -> Result<(), Box<std::error::Error>> {
    let m = App::new(crate_name!())
        .about(crate_description!())
        .version(crate_version!())
        .arg(
            Arg::with_name("settings")
                .index(1)
                .help("The settings file in TOML format")
                .long_help(r#"Refer to dist.settings.toml for an example configuration."#),
        )
        .get_matches();

    let s = m
        .value_of("settings")
        .map(Settings::new)
        .expect("path to settings file")?;

    let mut t = tera::Tera::new("templates/*")?;

    t.register_filter("nl_to_paragraph", view::filter::nl_to_paragraph);

    run(&s, &t)
}

fn run(s: &Settings, t: &tera::Tera) -> Result<(), Box<std::error::Error>> {
    let sql = if s.public {
        schema::Sql::public()
    } else {
        schema::Sql::private()
    };

    let pool = mysql::Pool::new(s.database_url())?;

    let forum_paths: HashMap<i32, String> = pool
        .prep_exec(sql.forum_names, ())
        .expect("sql forum names")
        .map(|result| result.expect("result to row"))
        .map(schema::ForumName::from_row)
        .map(|f| (f.forum_id, forum_path_of(&f)))
        .collect();

    let topic_paths: HashMap<i32, String> = pool
        .prep_exec(sql.topic_names, ())
        .expect("sql topic names")
        .map(|result| result.expect("result to row"))
        .map(schema::TopicName::from_row)
        .map(|t| (t.topic_id, topic_path_of(&t)))
        .collect();

    let post_topics: HashMap<i32, i32> = pool
        .prep_exec(sql.post_topics, ())
        .expect("sql post topics")
        .map(|result| result.expect("result to row"))
        .map(schema::PostTopic::from_row)
        .map(|p| (p.post_id, p.topic_id))
        .collect();

    let categories: Vec<schema::Forum> = pool
        .prep_exec(sql.list_forums, ())
        .expect("sql list forums")
        .map(|result| result.expect("result to row"))
        .map(schema::Forum::from_row)
        .collect();

    let mut fs = Vec::new();

    let categories: Vec<view::Category> = {
        let mut cs = Vec::with_capacity(11);
        let mut prev_cat = None;
        for c in categories {
            if prev_cat != Some(c.cat_id) {
                cs.push(view::Category {
                    cat_id: c.cat_id,
                    cat_title: c.cat_title.clone(),
                    forums: Vec::new(),
                });
                prev_cat = Some(c.cat_id);
            }
            if let Some(cat) = cs.last_mut() {
                fs.push(c.forum_id);
                let forum_path = forum_paths
                    .get(&c.forum_id)
                    .expect("name of forum")
                    .to_string();
                cat.forums.push(view::Forum {
                    forum_id: c.forum_id,
                    forum_name: c.forum_name,
                    forum_path,
                });
            };
        }
        cs
    };

    std::fs::create_dir(&s.out_dir).expect("create non-existent category directory");
    let out_dir = PathBuf::from(&s.out_dir);

    let mut ctx = tera::Context::new();
    ctx.insert("base_url", &s.base_url);
    ctx.insert("categories", &categories);
    write!(
        File::create(out_dir.join("index.html")).expect("recreate category index.html"),
        "{}",
        t.render("categories.html", &ctx)?
    )?;

    let ts: Vec<i32> = fs
        .par_iter()
        .flat_map(|forum_id| {
            let topics: Vec<view::Topic> = pool
                .prep_exec(sql.list_threads, params! { "forum_id" => forum_id })
                .expect("sql list threads")
                .map(|result| result.expect("result to row"))
                .map(schema::Topic::from_row)
                .map(|t| {
                    let topic_path = topic_paths
                        .get(&t.topic_id)
                        .unwrap_or_else(|| panic!("name of topic: {:?}", t))
                        .to_string();
                    view::Topic {
                        forum_id: t.forum_id,
                        topic_id: t.topic_id,
                        topic_type: t.topic_type,
                        username: t.username.clone(),
                        topic_title: t.topic_title.clone(),
                        topic_time: DateTime::<Utc>::from_utc(t.topic_time, Utc).to_string(),
                        forum_name: t.forum_name.clone(),
                        forum_description: t.forum_description.clone(),
                        topic_path,
                    }
                })
                .collect();

            let mut ctx = tera::Context::new();
            ctx.insert("base_url", &s.base_url);
            ctx.insert("topics", &topics);

            let forum_path = forum_paths.get(&forum_id).expect("name of forum");
            let forum_dir = out_dir.join(forum_path);
            std::fs::create_dir_all(&forum_dir).expect("create non-existent forum directory");
            write!(
                File::create(forum_dir.join("index.html")).expect("recreate forum index.html"),
                "{}",
                t.render("topics.html", &ctx).expect("render topics")
            )
            .expect("write topics");

            topics
        })
        .map(|topics| topics.topic_id)
        .collect();

    ts.par_iter().for_each(|topic_id| {
        let thread: Vec<view::Post> = pool
            .prep_exec(sql.assemble_thread, params! { "topic_id" => topic_id })
            .expect("sql assemble thread")
            .map(|result| result.expect("result to row"))
            .map(schema::Post::from_row)
            .map(|p| view::Post {
                username: p.username,
                post_id: p.post_id,
                topic_id: p.topic_id,
                topic_title: p.topic_title,
                forum_name: p.forum_name,
                forum_path: forum_paths
                    .get(&p.forum_id)
                    .expect("name of forum")
                    .to_string(),
                post_time: DateTime::<Utc>::from_utc(p.post_time, Utc).to_string(),
                body: bbcode(rewrite_forum_links(
                    &forum_paths,
                    &topic_paths,
                    &post_topics,
                    &s.base_url,
                    &p.body,
                )),
            })
            .collect();

        let mut ctx = tera::Context::new();
        ctx.insert("base_url", &s.base_url);
        ctx.insert("posts", &thread);

        let topic_dir = out_dir.join(topic_paths.get(topic_id).expect("name of topic"));
        std::fs::create_dir_all(&topic_dir).expect("create non-existent topic directory");
        write!(
            File::create(topic_dir.join("index.html")).expect("recreate topic index.html"),
            "{}",
            t.render("thread.html", &ctx).expect("render thread")
        )
        .expect("write thread");
    });

    std::fs::copy("templates/style.css", out_dir.join("style.css"))?;

    Ok(())
}

fn forum_path_of(f: &schema::ForumName) -> String {
    format!("forum/{}/{}", f.forum_id, slug::slugify(&f.forum_name))
}

fn topic_path_of(t: &schema::TopicName) -> String {
    let mut dos_counter = t.topic_id.to_string();
    dos_counter.truncate(2);
    let dos_counter: u8 = dos_counter
        .parse()
        .expect("first two digits of topic id fit in u8");
    format!(
        "topic/{:02}/{}/{}",
        dos_counter,
        t.topic_id,
        slug::slugify(&t.topic_title)
    )
}

fn bbcode(s: String) -> String {
    let mut res = s;
    for &(ref p, r) in PATTERNS.iter() {
        res = p.replace_all(&res, r).into_owned();
    }
    res
}

fn rewrite_forum_links(
    forum_paths: &HashMap<i32, String>,
    topic_paths: &HashMap<i32, String>,
    post_topics: &HashMap<i32, i32>,
    base_url: &str,
    s: &str,
) -> String {
    let r = Regex::new(r#"(?:http://)?(?:www.)?(?:escapefromunderdark|efupw).com/(?:old_)?forums/viewtopic.php\?t=(?P<tid>\d+)(?P<has_query>&)?"#).unwrap();
    let s = r
        .replace_all(&s, |caps: &regex::Captures| {
            let tid = caps.name("tid").unwrap().as_str().parse().unwrap();
            match topic_paths.get(&tid) {
                Some(path) => {
                    let maybe_query = caps.name("has_query").map(|_| "?").unwrap_or("");
                    format!("{}/{}/index.html{}", base_url, path, maybe_query,)
                }
                None => caps[0].to_string(),
            }
        })
        .into_owned();

    let r = Regex::new(r#"(?:http://)?(?:www.)?(?:escapefromunderdark|efupw).com/(?:old_)?forums/viewtopic.php\?p=(?P<pid>\d+)(?:#[^]]+)?"#).unwrap();
    let s = r
        .replace_all(&s, |caps: &regex::Captures| {
            let pid = caps.name("pid").unwrap().as_str().parse().unwrap();
            match post_topics.get(&pid) {
                Some(tid) => match topic_paths.get(&tid) {
                    Some(path) => format!("{}/{}/index.html#post-{}", base_url, path, pid),
                    None => caps[0].to_string(),
                },
                None => caps[0].to_string(),
            }
        })
        .into_owned();

    let r = Regex::new(r#"(?:http://)?(?:www.)?(?:escapefromunderdark|efupw).com/(?:old_)?forums/viewforum.php\?f=(?P<fid>\d+)"#).unwrap();
    let s = r
        .replace_all(&s, |caps: &regex::Captures| {
            let fid = caps.name("fid").unwrap().as_str().parse().unwrap();
            match forum_paths.get(&fid) {
                Some(path) => format!("{}/{}/index.html", base_url, path),
                None => caps[0].to_string(),
            }
        })
        .into_owned();

    let r = Regex::new(
        r#"(?:http://)?(?:www.)?(?:escapefromunderdark|efupw).com/(?:old_)?forums/index.php"#,
    )
    .unwrap();
    r.replace_all(&s, base_url).into_owned()
}

lazy_static! {
    static ref PATTERNS: [(Regex, &'static str); 20] = [
        (
            Regex::new(r#"(?i)\[(/?)b(?::[a-f0-9]+)?\]"#).unwrap(),
            "<${1}strong>"
        ),
        (
            Regex::new(r#"(?i)\[(/?)i(?::[a-f0-9]+)?\]"#).unwrap(),
            "<${1}em>"
        ),
        (
            Regex::new(r#"(?i)\[(/?)u(?::[a-f0-9]+)?\]"#).unwrap(),
            "<${1}u>"
        ),
        (
            Regex::new(r#"(?i)\[(/?)s(?::[a-f0-9]+)?\]"#).unwrap(),
            "<${1}strike>"
        ),
        (Regex::new(r#"\[\*(?::[a-f0-9]+)?\]"#).unwrap(), "<li>"),
        (
            Regex::new(r#"(?i)\[list=\d+(?::[a-f0-9]+)?\]"#).unwrap(),
            "<ol>"
        ),
        (
            Regex::new(r#"(?i)\[list(?::[a-f0-9]+)?\]"#).unwrap(),
            "<ul>"
        ),
        (
            Regex::new(r#"(?i)\[/list:([ou])?(?::[a-f0-9]+)?\]"#).unwrap(),
            "</${1}l>"
        ),
        (
            Regex::new(r#"(?i)\[(/?)quote(?::[a-f0-9]+)?\]"#).unwrap(),
            "<${1}blockquote>"
        ),
        (
            Regex::new(r#"(?i)\[quote(?::[a-f0-9]+)?="(?:<[^>]+>)?([^"]+?)(?:<[^>]+>)?"\]"#)
                .unwrap(),
            "<blockquote><cite>$1</cite>\n"
        ),
        (
            Regex::new(r#"(?i)\[size=(\d+)(?::[a-f0-9]+)?\]"#).unwrap(),
            r#"<span style="font-size: ${1}px;">"#
        ),
        (
            Regex::new(r#"(?i)\[/size(?::[a-f0-9]+)?\]"#).unwrap(),
            "</span>"
        ),
        (
            Regex::new(r#"(?i)\[code:1(?::[a-f0-9]+)?\]"#).unwrap(),
            "<pre><code>"
        ),
        (
            Regex::new(r#"(?i)\[/code(?:(?::[0-9]+)*:[a-f0-9]+)?\]"#).unwrap(),
            "</code></pre>"
        ),
        (
            Regex::new(r#"(?i)\[url\](.*?)\[/url\]"#).unwrap(),
            r#"<a href="$1" rel="nofollow">$1</a>"#
        ),
        (
            Regex::new(r#"(?i)\[url=([^]]+)\]"#).unwrap(),
            r#"<a href="$1" rel="nofollow">"#
        ),
        (Regex::new(r#"(?i)\[/url\]"#).unwrap(), "</a>"),
        (
            Regex::new(r#"(?i)\[color=([^]]+?)(?::[a-f0-9]+)?\]"#).unwrap(),
            r#"<span style="color: $1;">"#
        ),
        (
            Regex::new(r#"(?i)\[/color(?::[a-f0-9]+)?\]"#).unwrap(),
            "</span>"
        ),
        (
            Regex::new(r#"(?i)\[img(?::[a-f0-9]+)?\](.*?)\[/img(?::[a-f0-9]+)?\]"#).unwrap(),
            r#"<img src="$1" />"#
        ),
    ];
}

#[cfg(test)]
mod test {
    use super::*;

    fn bbcode(s: &str) -> String {
        super::bbcode(s.into())
    }

    #[test]
    fn topic_path_of_single_digit_topic_id() {
        let t = schema::TopicName {
            topic_id: 1,
            topic_title: "topic title".to_string(),
        };
        assert_eq!(topic_path_of(&t), "topic/01/1/topic-title");
    }

    #[test]
    fn topic_path_of_double_digit_topic_id() {
        let t = schema::TopicName {
            topic_id: 12,
            topic_title: "topic title".to_string(),
        };
        assert_eq!(topic_path_of(&t), "topic/12/12/topic-title");
    }

    #[test]
    fn topic_path_of_triple_digit_topic_id() {
        let t = schema::TopicName {
            topic_id: 123,
            topic_title: "topic title".to_string(),
        };
        assert_eq!(topic_path_of(&t), "topic/12/123/topic-title");
    }

    #[test]
    fn rewrite_forum_link() {
        let links = vec![
            (
                "[url=www.escapefromunderdark.com/forums/viewtopic.php?t=18327]this[/url]",
                "[url=https://some.base/topic/18/18327/lorem/index.html]this[/url]"
            ),
            (
                "[url=http://escapefromunderdark.com/forums/viewtopic.php?t=10327]this[/url]",
                "[url=https://some.base/topic/18/10327/ipsum/index.html]this[/url]"
            ),
            (
                "[url=http://www.escapefromunderdark.com/forums/viewtopic.php?t=25013&amp;sid=21bc17ae5cb773cb36f72cf0d7dbb597]this link.[/url]",
                "[url=https://some.base/topic/25/25013/dolor-sit/index.html?amp;sid=21bc17ae5cb773cb36f72cf0d7dbb597]this link.[/url]"
            ),
            (
                "[url=http://www.escapefromunderdark.com/forums/viewtopic.php?t=344]",
                "[url=https://some.base/topic/34/344/sit/index.html]"
            ),
            (
                "[url]http://www.escapefromunderdark.com/forums/viewtopic.php?t=9395[/url]",
                "[url]https://some.base/topic/93/9395/amet/index.html[/url]"
            ),
            (
                "[url=http://www.efupw.com/old_forums/viewtopic.php?t=29717]A tale of Shadow and Triumph[/url]",
                "[url=https://some.base/topic/29/29717/consectetur/index.html]A tale of Shadow and Triumph[/url]"
            ),
            (
                "[url=http://www.escapefromunderdark.com/forums/viewtopic.php?p=123793#123793]",
                "[url=https://some.base/topic/34/344/sit/index.html#post-123793]"
            ),
            (
                "[url=http://www.escapefromunderdark.com/forums/viewtopic.php?p=42#nonsense]",
                "[url=https://some.base/topic/34/344/sit/index.html#post-42]"
            ),
            (
                "[url=http://www.escapefromunderdark.com/forums/viewtopic.php?p=37#thread-does-not-exist]",
                "[url=http://www.escapefromunderdark.com/forums/viewtopic.php?p=37#thread-does-not-exist]"
            ),
            (
                "[url=http://www.escapefromunderdark.com/forums/viewforum.php?f=72]",
                "[url=https://some.base/forum/72/dm-questions-answers/index.html]"
            ),
            (
                "[url=http://www.escapefromunderdark.com/forums/index.php]",
                "[url=https://some.base]"
            ),
        ];

        let forum_paths: HashMap<i32, String> = [(72, "forum/72/dm-questions-answers".into())]
            .iter()
            .cloned()
            .collect();

        let topic_paths: HashMap<i32, String> = [
            (10327, "topic/18/10327/ipsum".into()),
            (18327, "topic/18/18327/lorem".into()),
            (25013, "topic/25/25013/dolor-sit".into()),
            (29717, "topic/29/29717/consectetur".into()),
            (344, "topic/34/344/sit".into()),
            (9395, "topic/93/9395/amet".into()),
        ]
        .iter()
        .cloned()
        .collect();

        let post_topics: HashMap<i32, i32> = [(123793, 344), (42, 344)].iter().cloned().collect();

        for (input, expect) in links {
            assert_eq!(
                rewrite_forum_links(
                    &forum_paths,
                    &topic_paths,
                    &post_topics,
                    "https://some.base",
                    input.into()
                ),
                expect
            );
        }
    }

    #[test]
    fn b_i_u_s() {
        assert_eq!(
            bbcode("[b:276a4a41da]hello, world[/b:276a4a41da]"),
            "<strong>hello, world</strong>"
        );
        assert_eq!(
            bbcode("[i:276a4a41da]hello, world[/i:276a4a41da]"),
            "<em>hello, world</em>"
        );
        assert_eq!(
            bbcode("[u:276a4a41da]hello, world[/u:276a4a41da]"),
            "<u>hello, world</u>"
        );
        assert_eq!(
            bbcode("[s:276a4a41da]hello, world[/s:276a4a41da]"),
            "<strike>hello, world</strike>"
        );
    }

    #[test]
    fn list_ordered() {
        assert_eq!(
            bbcode("[list=1:004f626312]hello[/list:o:004f626312]"),
            "<ol>hello</ol>"
        );
        assert_eq!(
            bbcode("[list=1:004f626312][*]hello[*]world[/list:o:004f626312]"),
            "<ol><li>hello<li>world</ol>"
        );
    }

    #[test]
    fn list_unordered() {
        assert_eq!(
            bbcode("[list:004f626312]hello[/list:u:004f626312]"),
            "<ul>hello</ul>"
        );
        assert_eq!(
            bbcode("[list:004f626312][*]hello[*]world[/list:u:004f626312]"),
            "<ul><li>hello<li>world</ul>"
        );
    }

    #[test]
    fn quote_named() {
        assert_eq!(
            bbcode(r#"[quote:be1bfc0a48="hello"]world[/quote:be1bfc0a48]"#),
            "<blockquote><cite>hello</cite>\nworld</blockquote>"
        );
    }

    #[test]
    fn quote_anonymous() {
        assert_eq!(
            bbcode(r#"[quote:be1bfc0a48]world[/quote:be1bfc0a48]"#),
            "<blockquote>world</blockquote>"
        );
    }

    #[test]
    fn size() {
        assert_eq!(
            bbcode("[size=18:90be455979]hello[/size:90be455979]"),
            r#"<span style="font-size: 18px;">hello</span>"#
        );
    }

    #[test]
    fn code() {
        assert_eq!(bbcode("[code]"), "[code]");
        assert_eq!(
            bbcode("[code:1:ebaeab5c38]DelayCommand&#40;0.1f, AssignCommand&#40;oPC, SetFacing&#40;DIRECTION_xxx&#41;&#41;&#41;;[/code:1:ebaeab5c38]"),
            "<pre><code>DelayCommand&#40;0.1f, AssignCommand&#40;oPC, SetFacing&#40;DIRECTION_xxx&#41;&#41;&#41;;</code></pre>"
        );
    }

    #[test]
    fn url_plain() {
        assert_eq!(
            bbcode("[url]http://myth-drannor.net/DlabraddathNet/z-Dlabraddath/Main.htm[/url]"),
            r#"<a href="http://myth-drannor.net/DlabraddathNet/z-Dlabraddath/Main.htm" rel="nofollow">http://myth-drannor.net/DlabraddathNet/z-Dlabraddath/Main.htm</a>"#
        );
    }

    #[test]
    fn url_named() {
        assert_eq!(
            bbcode("[url=http://en.wikipedia.org/wiki/Tracking_%28hunting%29]Tracking[/url]"),
            r#"<a href="http://en.wikipedia.org/wiki/Tracking_%28hunting%29" rel="nofollow">Tracking</a>"#
        );
    }

    #[test]
    fn color() {
        assert_eq!(
            bbcode("[color=red:90be455979]hello[/color:90be455979]"),
            r#"<span style="color: red;">hello</span>"#
        );
    }

    #[test]
    fn img() {
        assert_eq!(
            bbcode("[img:1f4b218e70]http://img185.imageshack.us/img185/357/nwn0001ani3.jpg[/img:1f4b218e70]"),
            r#"<img src="http://img185.imageshack.us/img185/357/nwn0001ani3.jpg" />"#
        );
    }

    #[test]
    fn quoted_complex_list_int_test() {
        let input = r#"
[quote:39c2c2449f="Blackguard"][list:39c2c2449f][*:39c2c2449f]10% movement speed increase feat disabled if in heavy amour.

[*:39c2c2449f]Barbarian Damage Immunity, disabled in heavy amour.
[*:39c2c2449f]Level 5: 10% damage immunity.

[*:39c2c2449f]Level 8: 20% movement speed bonus, disabled in heavy amour.

[*:39c2c2449f]Level 8 rage is uncapped.[/list:u:39c2c2449f][/quote:39c2c2449f]
"#;
        let output = r#"
<blockquote><cite>Blackguard</cite>
<ul><li>10% movement speed increase feat disabled if in heavy amour.

<li>Barbarian Damage Immunity, disabled in heavy amour.
<li>Level 5: 10% damage immunity.

<li>Level 8: 20% movement speed bonus, disabled in heavy amour.

<li>Level 8 rage is uncapped.</ul></blockquote>
"#;
        assert_eq!(bbcode(input), output);
    }
}
